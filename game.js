var mark = 'X'

function changeMark(){
    if (mark == 'X') {
        mark = 'O'
    } else {
        mark = 'X'
    }
}

var gameField = {
    table: [[null,null,null,null], [null,null,null,null], [null,null,null,null], [null,null,null,null]],

    updateTableWithMark: function(row, column){
        this.table[row][column] = mark;
        document.getElementById(arguments[2]).innerHTML = mark;
        this.checkWinner();
    },

    checkTableForMark: function(row, column){
        return this.table[row][column] === null;
    },

    getIdAndUpdateTable: function(id) {
        var row = id.substring(0,1) - 1;
        var column = id.substring(2,3) - 1;
        if (this.checkTableForMark(row, column)){
            this.updateTableWithMark(row, column, id);
            changeMark();
        }
    },

    checkWinner: function(){
        let checkTable = [[],[],[],[],[],[],[],[],[],[]];
        let nullCheck = 0;

        for (let i = 0; i < this.table.length; i++){
            
            if (this.table[i].includes(null)) nullCheck++;
            for (let j = 0; j < this.table[i].length; j++){
                checkTable[i].push(this.table[i][j]);
                checkTable[i + 4].push(this.table[j][i]);
                if (i === j) checkTable[8].push(this.table[i][j]);
                if (i === (j - 4)) checkTable[9].push(this.table[i][j]);
            }
        }
        for (let i = 0; i < checkTable.length; i++){
            // if (this.table[i].includes(null)) break;
            for (let j = 0; j < checkTable[i].length; j++){
                // checkTable[i].sort();
                console.log(`${i} ${j} ${checkTable[i][j]}`);

            }
        } 

        checkTable.forEach(arr => {
            arr.sort();
            if (arr[0] !== null && arr[0] !== undefined && arr[0] === arr[arr.length - 1]){
                console.log(`${arr[0]} ${arr[arr.length - 1]}`);
                alert(`Winner mark ${mark}`);
            }
        });

        if (nullCheck === 0){
            alert("No one cool, suckers");
        }
        nullCheck = 0;
        // checkTable.forEach(arr => {
        //     if (arr.every(element => element === mark)) alert("winner mark " + mark)
        // });
    }
}

function markInsert(event){
    if (event.target.classList.contains('mark_cell')){
        gameField.getIdAndUpdateTable(event.target.id);
    }
}

document.addEventListener('click', markInsert, false);